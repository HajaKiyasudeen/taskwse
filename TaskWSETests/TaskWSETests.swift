//
//  TaskWSETests.swift
//  TaskWSETests
//
//  Created by Apple on 02/06/2021.
//

import XCTest
@testable import TaskWSE

class TaskWSETests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testChartDataUnitMoreThanMaxValue() {
        //Given
        let chartDataFromJSON = ChartInputData.loadChartsData(from: "ChartData_test_more_than_10_unit") ?? []
        let viewModel = ChartViewModel()
        //When
        let filteredData = viewModel.lesserThanMaxUnitData(with: chartDataFromJSON)
        //Then
        XCTAssert(chartDataFromJSON.count == 11, "Chart array count doesn't match with JSON input")
        XCTAssert(filteredData.count == 8, "Filterd chart array count doesn't match with lesser than 10 unit input")
    }
    
    func testChartDataValid() {
        //Given
        let chartDataFromJSON = ChartInputData.loadChartsData(from: "ChartData_test_valid") ?? []
        let viewModel = ChartViewModel()
        //When
        let filteredData = viewModel.validUnitChartData(with: chartDataFromJSON)
        //Then
        XCTAssert(chartDataFromJSON.count == 11, "Chart array count doesn't match with JSON input")
        XCTAssert(filteredData.count == 11, "Filterd chart array doesn't have valid data")
    }
    
    func testChartDataWithInValidMonthValue() {
        //Given
        var chartDataFromJSON = ChartInputData.loadChartsData(from: "ChartData_test_valid") ?? []
        let viewModel = ChartViewModel()
        //When
        chartDataFromJSON[1].Month = nil
        chartDataFromJSON[9].Month = nil
        chartDataFromJSON[10].Month = nil
        let filteredData = viewModel.validUnitChartData(with: chartDataFromJSON)
        //Then
        XCTAssert(chartDataFromJSON.count == 11, "Chart array count doesn't match with JSON input")
        XCTAssert(filteredData.count == 8, "Filterd chart array count doesn't match with month with non null data")
    }

}
