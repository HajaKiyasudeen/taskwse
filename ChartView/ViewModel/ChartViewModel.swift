//
//  ChartViewModel.swift
//  TaskWSE
//
//  Created by Apple on 02/06/2021.
//

import Foundation
import Charts

class ChartViewModel: NSObject {
    
    var maxYAxis: Double = 10
    
    func xAxisMonthArray() -> [String]{
        return ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }
    
    fileprivate func chartDataArray() -> [ChartInputData] {
        return ChartInputData.loadChartsData(from: "ChartData") ?? []
    }
    
    func percentageToUnit(_ percentage: Double) -> Double {
        return abs(percentage/maxYAxis)
    }
    
    func validUnitChartData(with array: [ChartInputData]) -> [ChartData] {
        let lesserThan10UnitArray = lesserThanMaxUnitData(with: array)
        var returnArray: [ChartData] = []
        
        for loopValue in lesserThan10UnitArray {
            guard let haveMonth = loopValue.Month,
                  haveMonth.count > 0 else { continue }
            guard let haveUnit = loopValue.Unit else { continue }
            guard let haveProgressValue = loopValue.ProgressPercentage else { continue }
            
            let currentValue = ChartData(Month: haveMonth, Unit: Double(abs(haveUnit)), ProgressPercentage: Double(haveProgressValue <= 100 ? Double(haveProgressValue):0.0))
            returnArray.append(currentValue)
        }
        
        return returnArray
    }
    
    func lesserThanMaxUnitData(with array: [ChartInputData]) -> [ChartInputData] {
        return array.filter( { $0.Unit ?? 0 <= Int(maxYAxis) })
    }
    
    func barChartValues(for array: [ChartData]) -> [BarChartDataEntry] {
        var dataEntries: [BarChartDataEntry] = []
        let monthArray = xAxisMonthArray()
        
        for loopValue in array {
            if let monthIndex = monthArray.firstIndex(of: loopValue.Month) {
                let barChartForAData = BarChartDataEntry(x: Double(monthIndex), y: percentageToUnit(loopValue.ProgressPercentage))
                dataEntries.append(barChartForAData)
            }
        }
        
        return dataEntries
    }
    
    func barChartData() -> BarChartData {
        let dataArray = barChartValues(for: validUnitChartData(with: chartDataArray()))
        let chartDataSet = BarChartDataSet(entries: dataArray, label: "Progress")
        chartDataSet.colors = [UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        return BarChartData(dataSets: [chartDataSet])
    }
    
}
