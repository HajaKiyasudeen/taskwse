//
//  ChartViewController.swift
//  TaskWSE
//
//  Created by Apple on 02/06/2021.
//

import UIKit
import Charts

class ChartViewController: UIViewController {

    fileprivate let viewModel = ChartViewModel()
    
    fileprivate var barChartView: BarChartView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        setUpUI()
    }
    
    //MARK:- Set Up UI
    func setUpUI() {
        setUpBarChartView()
        setUpConstraints()
    }
    
    //MARK:- SetUp BarChartView
    func setUpBarChartView() {
        //barChartView
        barChartView = BarChartView()
        barChartView.translatesAutoresizingMaskIntoConstraints = false
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.data = viewModel.barChartData()
        barChartView.backgroundColor = UIColor(red: 189/255, green: 195/255, blue: 199/255, alpha: 1)
        barChartView.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .linear)
        barChartView.rightAxis.enabled = false
        //barChartView.delegate = self
        view.addSubview(barChartView)
        
        setupXAxis()
        setupYAxis()
    }
    
    func setupXAxis() {
        let xAxisArray = viewModel.xAxisMonthArray()
        let xaxis = barChartView.xAxis
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bottom
        xaxis.valueFormatter = IndexAxisValueFormatter(values: xAxisArray)
        xaxis.granularity = 1
        xaxis.labelTextColor = UIColor.black
        xaxis.labelCount = xAxisArray.count
    }
    
    func setupYAxis() {
        let yaxis = barChartView.leftAxis
        yaxis.spaceTop = 0.35
        yaxis.axisMinimum = 0
        yaxis.axisMaximum = viewModel.maxYAxis
        yaxis.drawGridLinesEnabled = false
        yaxis.labelTextColor = UIColor.black
    }
    
    //MARK:- Set Up Constraints
    func setUpConstraints() {
        //barChartView
        barChartView.leadingAnchor.constraint(equalTo: barChartView.superview!.leadingAnchor,constant: 20).isActive = true
        barChartView.trailingAnchor.constraint(equalTo: barChartView.superview!.trailingAnchor,constant: -20).isActive = true
        barChartView.topAnchor.constraint(equalTo: barChartView.superview!.topAnchor,constant: 150).isActive = true
        barChartView.bottomAnchor.constraint(equalTo: barChartView.superview!.bottomAnchor,constant: -150).isActive = true

    }
}

