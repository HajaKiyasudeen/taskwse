//
//  ChartInputData.swift
//  TaskWSE
//
//  Created by Apple on 02/06/2021.
//

import Foundation

struct ChartInputData: Codable {
    
    var Month : String?
    var Unit : Int?
    var ProgressPercentage : Int?

    static func loadChartsData(from fileName: String) -> [ChartInputData]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([ChartInputData].self, from: data)
                
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}

struct ChartData: Codable {
    var Month : String
    var Unit : Double
    var ProgressPercentage : Double
}
